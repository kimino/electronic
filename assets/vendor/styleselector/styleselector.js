/*-----------------------------------------------------------------------------------
 /* Styles Switcher
 -----------------------------------------------------------------------------------*/

window.console = window.console || (function(){
	var c = {}; c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function(){};
	return c;
})();


jQuery(document).ready(function($) {

	//// Skin
	//$(".sk-w" ).click(function(){
	//	$("#skin" ).attr("href", "css/style.css" );
	//	return false;
	//});
    //
	//$(".sk-d" ).click(function(){
	//	$("#skin" ).attr("href", "css/dark.css" );
	//	return false;
	//});

	// Color Changer
	$(".contrast" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_contrast.css" );
		return false;
	});

	$(".blue-grey" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_blue-grey.css" );
		return false;
	});

	$(".brown" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_brown.css" );
		return false;
	});

	$(".camp" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_camp.css" );
		return false;
	});

	$(".cyan" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_cyan.css" );
		return false;
	});

	$(".grass" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_grass.css" );
		return false;
	});

	$(".miami" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_miami.css" );
		return false;
	});

	$(".orange" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_orange.css" );
		return false;
	});

	$(".pink" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_pink.css" );
		return false;
	});

	$(".times" ).click(function(){
		$("#colors" ).attr("href", "assets/custom/css/style_times.css" );
		return false;
	});


	$("#style-switcher h2 a").click(function(e){
		e.preventDefault();
		var div = $("#style-switcher");
		console.log(div.css("left"));
		if (div.css("left") === "-206px") {
			$("#style-switcher").animate({
				left: "0px"
			});
		} else {
			$("#style-switcher").animate({
				left: "-206px"
			});
		}
	});

	// Footer Style Switcher
	$("#footer-style").change(function(e){
		if( $(this).val() == 1){
			$("#footer").removeClass("dark");
			$("#footer-bottom").removeClass("dark");
		} else{
			$("#footer").addClass("dark");
			$("#footer-bottom").addClass("dark");
		}
	});

	//Layout Switcher
	$("#layout-style").change(function(e){
		if( $(this).val() == 1){
			$("body").removeClass("boxed"),
            $("#header-wrapper").removeClass("boxed-hero"),
				$(window).resize();
			//stickyheader = !stickyheader;
		} else{
			$("body").addClass("boxed"),
            $("#header-wrapper").addClass("boxed-hero"),
				$(window).resize();
			//stickyheader = !stickyheader;
		}
	});

	$("#layout-switcher").on('change', function() {
		$('#layout').attr('href', $(this).val() + '.css');
	});

	$(".colors li a").click(function(e){
		e.preventDefault();
		$(this).parent().parent().find("a").removeClass("active");
		$(this).addClass("active");
	});

	$('.bg li a').click(function() {
		var current = $('#style-switcher select[id=layout-style]').find('option:selected').val();
		if(current == '2') {
			var bg = $(this).css("backgroundImage");
			$("body").css("backgroundImage",bg);
		} else {
			alert('Please select boxed layout!');
		}
	});

	$("#reset a").click(function(e){
		var bg = $(this).css("backgroundImage");
		$("body").css("backgroundImage","url(./images/bg/noise.png)");
		$("#navigation" ).removeClass("style-2")
	});


});